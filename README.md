# Semantic Search with Transformers and Elasticsearch

This project is an implementation of a semantic search engine using the Hugging Face Transformers library and Elasticsearch. Specifically, it uses the DistilBERT model fine-tuned on NLI and STS-B tasks for sentence embeddings.

## Dependencies

- Python 3.8+
- Transformers
- Elasticsearch
- Pandas

You can install the necessary libraries with:

```bash
pip install elasticsearch transformers pandas
```

## Project Structure
main.py: The main script that includes data acquisition and processing, loading and tokenizing with the transformer model, computing sentence embeddings, indexing in Elasticsearch, and querying the Elasticsearch index.

## Data
We use the "20 Newsgroups" dataset in this example, which is fetched from sklearn datasets. You can replace this with your own data.

## Usage
To run the program:
```bash
cd c:\elasticsearch-7.17.12
.\bin\elasticsearch.bat
python semantic_search.py
```
## To use the search function:
```bash
from semantic_search import search

results = search("my search query")
for text, score in results:
    print(f"Score: {score}\nText: {text}\n")
```

