from sklearn.datasets import fetch_20newsgroups
import pandas as pd
from semantic_search import SemanticSearch

import torch
torch.cuda.empty_cache()

# Fetch data
newsgroups_train = fetch_20newsgroups(subset='train')
data = pd.DataFrame({'text': newsgroups_train.data}).head(100)
#data = data['text'].tolist()
print(data[:3])

ss = SemanticSearch(data)

# Use the search function
results = ss.search("lerxst")
for text, score in results:
    print(f"Score: {score}\nText: {text}\n")
