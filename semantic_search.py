from elasticsearch import Elasticsearch, RequestError
import torch
from transformers import AutoTokenizer, AutoModel
import numpy as np

# Define mapping
mapping = {
    "mappings": {
        "properties": {
            "text": {
                "type": "text"
            },
            "embedding": {
                "type": "dense_vector",
                "dims": 768
            }
        }
    }
}


class SemanticSearch:
    def __init__(self, data):
        self.es = None
        self.batch_size = 32

        # Initialize the model and tokenizer
        model_name = 'sentence-transformers/distilbert-base-nli-stsb-mean-tokens'
        self.tokenizer = AutoTokenizer.from_pretrained(model_name)
        self.model = AutoModel.from_pretrained(model_name).to('cuda')
        self.mean_embeddings = self.create_mean_embeddings(data)
        # Initialize Elasticsearch client
        self.create_es_table(data)



    def create_mean_embeddings(self, data):
        embeddings = []
        for i in range(0, len(data), self.batch_size):
            batch = data[i:i + self.batch_size]
            tokenized_batch = self.tokenizer(batch['text'].tolist(), padding=True, truncation=True, max_length=512,
                                             return_tensors='pt').to('cuda')

            with torch.no_grad():
                model_output = self.model(**tokenized_batch)

            # We use mean pooling to get one fixed sized sentence vector
            input_mask_expanded = tokenized_batch['attention_mask'].unsqueeze(-1).expand(
                model_output.last_hidden_state.size()).float()
            sum_embeddings = torch.sum(model_output.last_hidden_state * input_mask_expanded, 1)
            sum_mask = torch.clamp(input_mask_expanded.sum(1), min=1e-9)
            mean_embeddings = sum_embeddings / sum_mask

            embeddings.append(mean_embeddings.cpu().numpy())

        return np.concatenate(embeddings)

    def create_es_table(self, data):
        global mapping
        self.es = Elasticsearch('http://localhost:9200')  # "http://localhost:9200"
        # Create index
        index_name = "my_index"
        self.es.indices.create(index=index_name, body=mapping)


        # Index documents
        for i, row in data.iterrows():
            document = {
                "text": row['text'],
                "embedding": self.mean_embeddings[i].tolist()
            }
            self.es.index(index=index_name, id=i, body=document)

    def search(self, query):
        # Compute query embedding
        tokenized_query = self.tokenizer(query, padding=True, truncation=True, max_length=512, return_tensors='pt').to(
            'cuda')
        with torch.no_grad():
            model_output = self.model(**tokenized_query)
        query_embedding = torch.mean(model_output.last_hidden_state, dim=1).cpu().numpy()

        # Define script for cosine similarity
        script_query = {
            "script_score": {
                "query": {"match_all": {}},
                "script": {
                    "source": "cosineSimilarity(params.query_vector, 'embedding') + 1.0",
                    "params": {"query_vector": query_embedding.tolist()[0]}
                }
            }
        }

        # Execute search query
        response = self.es.search(index="my_index", body={"size": 5, "query": script_query})

        return [(hit['_source']['text'], hit['_score']) for hit in response['hits']['hits']]
